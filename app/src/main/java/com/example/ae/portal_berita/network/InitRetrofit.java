package com.example.ae.portal_berita.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ae on 16-Jun-18.
 */

public class InitRetrofit {
    public static String API_URL = "http://192.168.43.4/portal/";
    public static Retrofit setInit() {
        return new Retrofit.Builder().baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
    public static ApiServices getInstance() {
        return setInit().create(ApiServices.class);
    }
}
