package com.example.ae.portal_berita.networkstta;

import com.example.ae.portal_berita.responsestta.ResponseBeritaStta;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by ae on 20-Jun-18.
 */

public interface ApiServicesstta {

    @GET("stta_tampil.php")
    Call<ResponseBeritaStta> request_show_all_berita();

}
