package com.example.ae.portal_berita.network;

import com.example.ae.portal_berita.response.ResponseBerita;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by ae on 16-Jun-18.
 */

public interface ApiServices {
    @GET("mhs_tampil.php")
    Call<ResponseBerita> request_show_all_berita();
}
