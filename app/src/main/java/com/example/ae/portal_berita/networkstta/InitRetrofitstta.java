package com.example.ae.portal_berita.networkstta;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ae on 20-Jun-18.
 */

public class InitRetrofitstta {
    public static String API_URL = "http://192.168.43.4/portal/";
    public static Retrofit setInit() {
        return new Retrofit.Builder().baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static ApiServicesstta getInstance() {
        return setInit().create(ApiServicesstta.class);
    }
}
