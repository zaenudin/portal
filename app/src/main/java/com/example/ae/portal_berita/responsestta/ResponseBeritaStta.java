package com.example.ae.portal_berita.responsestta;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class ResponseBeritaStta{

	@SerializedName("berita")
	private List<BeritaItemStta> berita;

	@SerializedName("status")
	private boolean status;

	public void setBerita(List<BeritaItemStta> berita){
		this.berita = berita;
	}

	public List<BeritaItemStta> getBerita(){
		return berita;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ResponseBeritaStta{" + 
			"berita = '" + berita + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}