package com.example.ae.portal_berita;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.ae.portal_berita.network.ApiServices;
import com.example.ae.portal_berita.network.InitRetrofit;
import com.example.ae.portal_berita.networkstta.ApiServicesstta;
import com.example.ae.portal_berita.networkstta.InitRetrofitstta;
import com.example.ae.portal_berita.response.BeritaItem;
import com.example.ae.portal_berita.response.ResponseBerita;
import com.example.ae.portal_berita.responsestta.BeritaItemStta;
import com.example.ae.portal_berita.responsestta.ResponseBeritaStta;

import java.io.File;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivityStta extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView recyclerView;
    private ShareActionProvider shareActionProvider;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_stta);

        getSupportActionBar().setTitle("Pengenalan STTA");
        //getSupportActionBar().setSubtitle("Pengenalan Kampus STTA");
        mTitle = mDrawerTitle = getTitle();
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.rvListBerita_STTA);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerlayout_STTA);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.navigation_drawer_open,R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView)findViewById(R.id.nav_view_STTA);
        navigationView.setNavigationItemSelectedListener(this);


        tampilBerita();
    }

    private void tampilBerita() {
        ApiServicesstta api = InitRetrofitstta.getInstance();
        Call<ResponseBeritaStta> beritaCall = api.request_show_all_berita();
        beritaCall.enqueue(new Callback<ResponseBeritaStta>() {
            @Override
            public void onResponse(Call<ResponseBeritaStta> call, Response<ResponseBeritaStta> response) {
                if (response.isSuccessful()){
                    Log.d("response api", response.body().toString());
                    List<BeritaItemStta> data_berita = response.body().getBerita();
                    boolean status = response.body().isStatus();
                    if (status){
                        AdapterBeritaStta adapter = new AdapterBeritaStta(MainActivityStta.this, data_berita);
                        recyclerView.setAdapter(adapter);
                    } else {
                        Toast.makeText(MainActivityStta.this, "Tidak ada berita untuk saat ini", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBeritaStta> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_shere, menu);

        MenuItem item = menu.findItem(R.id.share);
        shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
        setShare();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.rate:
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "indonesia.developer.net.mobile_cbt_&hl=in")));
                } catch (ActivityNotFoundException ex) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=indonesia.developer.net.mobile_cbt_&hl=in")));
                }
                break;
            case R.id.share:
                setShare();
                break;
        }

        return true;
    }

    private void setShare() {
        ApplicationInfo appInfo = getApplicationContext().getApplicationInfo();
        String apkPath = appInfo.sourceDir;
        Intent Share = new Intent();
        Share.setAction(Intent.ACTION_SEND);
        Share.setType("application/vnd.android.package-archive");
        Share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(apkPath)));
        shareActionProvider.setShareIntent(Share);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.nav_home_mhs) {
            startActivity(new Intent(MainActivityStta.this, MainActivity.class));
        }else if (id == R.id.nav_home_stta){
            startActivity(new Intent(MainActivityStta.this,MainActivityStta.class));
            //Toast.makeText(MainActivityStta.this,"On Click Home STTA",Toast.LENGTH_SHORT).show();
        }else if (id == R.id.nav_person){
            Toast.makeText(MainActivityStta.this,"On Click Jurusan STTA",Toast.LENGTH_SHORT).show();
        }else if (id == R.id.nav_flight){
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivityStta.this);
            builder.setMessage("Walcome UKM STTA, Thankyou").setNegativeButton("OK",null).show();
        }else if (id == R.id.nav_email){
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivityStta.this);
            builder.setMessage("Walcome to Email STTA, Thank's").setNegativeButton("OK",null).show();
        }

        return false;
    }
}
